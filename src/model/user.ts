export class User {
  username?: string;
  age?: number;
  password?: string;
  isAdmin?: boolean;
}