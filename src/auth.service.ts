import { Injectable } from '@angular/core';
import { User } from './model/user';
import { HttpClient } from "@angular/common/http"
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false;

  constructor(private http: HttpClient) { }

  // store current user to local storage & save it on server
  createUser(user: User) {
    user.password = this.calcHash(user.password).toString();

    localStorage.setItem(user.username, JSON.stringify(user));

    this.http.post<any[]>('http://localhost:4201/users', { user })
      .subscribe(next => console.log(next));
  }

  // check if password is correct
  checkLogin(user: User) {
    user.password = this.calcHash(user.password).toString();

    var foundUser = JSON.parse(localStorage.getItem(user.username));
    if(foundUser != null){
      if(foundUser.password == user.password){
        return true;
      } 
    }
    return false;
  }

  // hash calculator that data on server encrypted
  calcHash(s) {
    for (var i = 0, h = 0xdeadbeef; i < s.length; i++)
      h = Math.imul(h ^ s.charCodeAt(i), 2654435761);
    return (h ^ h >>> 16) >>> 0;
  };
}
