import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { LoginComponent } from 'src/ui/login/login.component';
import { CreateUserComponent } from 'src/ui/create-user/create-user.component';
import { HomeComponent } from 'src/ui/home/home.component';
import { AuthGuard } from './auth.guard'

const routes: Routes = [
  { 
    path: '',
    component: LoginComponent },
  { 
    path: 'login', 
    component: LoginComponent },
  { 
    path: 'createUser', 
    component: CreateUserComponent },
  {
    path: 'home',
    component: HomeComponent, 
    canActivate: [AuthGuard] // security breach & solution
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
