import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from 'src/ui/login/login.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CreateUserComponent } from 'src/ui/create-user/create-user.component';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { AngularWebStorageModule } from 'angular-web-storage';
import { StorageModule } from '@ngx-pwa/local-storage';
import { HomeComponent } from 'src/ui/home/home.component';
import { AuthGuard } from './auth.guard';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateUserComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    InputTextModule,
    ButtonModule,
    CommonModule,
    MessagesModule,
    MessageModule,
    FormsModule,
    CheckboxModule,
    AngularWebStorageModule,
    StorageModule.forRoot({ IDBNoWrap: true })
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
