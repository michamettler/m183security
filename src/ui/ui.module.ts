import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { HomeComponent } from './home/home.component';



@NgModule({
  declarations: [LoginComponent, CreateUserComponent, HomeComponent],
  imports: [
    CommonModule
  ]
})
export class UiModule { }
