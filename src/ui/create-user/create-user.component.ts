import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/model/user';
import { AuthService } from 'src/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/message';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
  providers: [MessageService]
})
export class CreateUserComponent implements OnInit {
  user: User = {};
  statusPasswordLength = false;
  statusPasswordSpecial = false;
  statusPasswordUpper = false;

  statusUsernameNotUnique = false;
  statusUsernameEmpty = false;

  success = false;

  constructor(private http: HttpClient, private auth: AuthService, private router: Router,
    private messageService: MessageService) { }

  ngOnInit() {
  }

  // method to create a user if input data is valid
  createUser(event) {
    event.preventDefault();
    const target = event.target;
    var valid = false;

    this.checkPassword(target.password.value);
    this.checkUsername(target.username.value);

    if (!this.statusPasswordLength && !this.statusPasswordSpecial && !this.statusPasswordUpper
      && !this.statusUsernameNotUnique && !this.statusUsernameEmpty) {
      valid = true;
    }

    if (valid) {
      this.user.username = target.username.value;
      this.user.password = target.password.value;
      this.user.age = target.age.value;

      this.auth.createUser(this.user);

      this.success = true;
    }
  }

  // error message handling if input data is invalid (password)
  checkPassword(password: string) {
    if (password.length < 10) {
      this.statusPasswordLength = true;
    }

    if (!this.hasSpecialCharacters(password)) {
      this.statusPasswordSpecial = true;
    }

    if (!this.hasUpperLetters(password)) {
      this.statusPasswordUpper = true;
    }
  }

  // error message handling if input data is invalid (username)
  checkUsername(username: string) {
    if (username == '') {
      this.statusUsernameEmpty = true;
    }

    var foundUser = localStorage.getItem(username);
    if (foundUser != null) {
      this.statusUsernameNotUnique = true;
    }
  }

  // reset error messages for password field
  resetPassword() {
    this.statusPasswordLength = false;
    this.statusPasswordSpecial = false;
    this.statusPasswordUpper = false;
  }

  // reset error messages for username field
  resetUsername() {
    this.statusUsernameEmpty = false;
    this.statusUsernameNotUnique = false;
  }

  hasSpecialCharacters(str) {
    var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    return (format.test(str));
  }

  hasUpperLetters(str) {
    return (/[A-Z]/.test(str));
  }

}
