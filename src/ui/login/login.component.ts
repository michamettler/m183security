import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = {};
  error = false;

  constructor(private http: HttpClient, private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  // login method to check if login data is correct
  login(event) {
    const target = event.target;

    this.user.username = target.username.value;
    this.user.password = target.password.value;

    var valid = this.auth.checkLogin(this.user);

    if(valid){
      this.auth.loggedInStatus = true;
      this.router.navigate(['home']);
    } else {
      this.error = true;
    }
  }

}
