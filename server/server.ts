import { Type } from "@angular/compiler";

const express = require('express');
const app = express();

var fs = require('fs')
var morgan = require('morgan')
var path = require('path')

var fs = require("fs");

// setup logger
var accessLogStream = fs.createWriteStream(path.join(__dirname, '../log/access.log'), { flags: 'a' })
app.use(morgan('combined', { stream: accessLogStream }))

var user;

// setup express server
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        console.log('${req.ip} ${req.method} ${req.url}');
        next();
    }
});

app.use(express.json());

app.listen(4201, '127.0.0.1', function () {
    console.log('Sever now listening on 4201');
});

// get method for last created user (visbile on server (localhost:4201/users))
app.get('/users', (req, res) => {
    if (this.user) { //backend validation
        res.send([{
            object: "username: " + this.user[0] +
                ", password (hashed): " + this.user[1] +
                ", age: " + this.user[2] +
                ", isAdmin: " + this.user[3]
        }])
    } else {
        res.send([{ message: "no current user to display" }]);
    }
});

// post method to create new user
app.post('/users', (req, res) => {
    if(req.body.user.username != '' && req.body.user.username != ''){ //backend validation
        res.send({ body: req.body });

        this.user = [];
        this.user.push(req.body.user.username);
        this.user.push(req.body.user.password);
        this.user.push(req.body.user.age);
        this.user.push(req.body.user.isAdmin);
    
        writeFile(this.user[0], this.user[1], this.user[2], this.user[3]);
    }
});

function writeFile(username: String, password: String, age: number, isAdmin: boolean) {

    var user = {
        username: username,
        password: password,
        age: age,
        isAdmin: isAdmin
    };

    fs.writeFile("./db/database.json", JSON.stringify(user, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log("File has been created");
    });
}

